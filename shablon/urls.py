# -*- coding: utf-8 -*-

from django.conf.urls import *
# Uncomment the next two lines to enable the admin:
from . import views

urlpatterns = [
    url(r'', views.index, name='index'),
    # ... your url patterns
]
